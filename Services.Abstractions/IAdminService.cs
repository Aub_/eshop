﻿using Contracts;
using Contracts.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface IAdminService
    {
        List<AdminDTO> GetAll();
        AdminDTO GetById(int AdminId);
        AdminDTO GetByEmail(string Email);
        void Add(List<AdminDTO> NewAdmins);
        AdminDTO Update(AdminDTO UpdatedAdmin);
        void Delete(List<int> DeletedAdminIds);

        // Account 
        AdminDTO Login(LoginDTO loginDTO);
        string SetPasswordResetToken(AdminDTO admin);
        bool ResetPassword(ResetPasswordModel model);
        public AdminDTO ChangePassword(string newPassword, string oldPassword, AdminDTO admin);

        // // Refresh Tokens
        RefreshToken CreateRefreshToken(int id);
        JWTModel RefreshToken(JWTModel oldTokens);
        bool RevokeRefreshToken(int id);
    }
}
