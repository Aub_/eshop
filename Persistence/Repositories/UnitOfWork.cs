﻿using Domain.Repositories;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ShopContext _shopDBContext;

        public UnitOfWork(ShopContext shopDBContext)
        {
            _shopDBContext = shopDBContext;
        }

        public int SaveChanges()=>
             _shopDBContext.SaveChanges();
       
    }
}
