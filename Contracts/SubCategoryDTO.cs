﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public class SubCategoryDTO
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public List<ProductDTO> Products { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
