﻿using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ShopContext _shopContext;

        public CategoryRepository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public void Add(List<Category> categories)
        {
            _shopContext.Categories.AddRange(categories);
        }

        public void Delete(List<Category> categories)
        {
            _shopContext.Categories.RemoveRange(categories);
        }

        public List<Category> GetAll()
        {
            return _shopContext.Categories.ToList();
        }

        public Category GetById(int categoryId)
        {
            return _shopContext.Categories.Find(categoryId);
        }

        public  List<Category> GetByIds(List<int> categoryIds)//TODO fix bug
        {
            try
            {
                var x = _shopContext.Categories.Where(c => categoryIds.Contains(c.ID)).ToList();
                return x;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Category> GetByName(string name)
        {
            return _shopContext.Categories.Where(c => c.Name.Contains(name)).ToList();
        }

        public Category Update(Category original , Category updated)
        {
            original.Name = updated.Name;
            return original;
        }
    }
}
