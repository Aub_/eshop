﻿using Domain.Entities;
using Domain.Repositories;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    class AdminRepository : IAdminRepository
    {
        private readonly ShopContext _shopContext;

        public AdminRepository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public bool Activate(Admin admin)
        {
            admin.IsActive = !admin.IsActive;
            return admin.IsActive;
        }

        public void Add(List<Admin> NewAdmins)
        {
            _shopContext.Admins.AddRange(NewAdmins);
        }

        public void Delete(List<Admin> DeletedAdmins)
        {
            _shopContext.Admins.RemoveRange(DeletedAdmins);
        }

        public List<Admin> GetAll()
        {
            return _shopContext.Admins.ToList();
        }

        public Admin GetByEmail(string Email)
        {
            return _shopContext.Admins.Where(a => a.Email == Email).FirstOrDefault();
        }

        public Admin GetById(int Id)
        {
            return _shopContext.Admins.Where(a => a.ID == Id).FirstOrDefault();
        }

        public List<Admin> GetByIds(List<int> Ids)
        {
            try
            {
                return _shopContext.Admins.Where(a => Ids.Contains(a.ID)).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Admin> GetByName(string Name)
        {
            return _shopContext.Admins.Where(a => a.FirstName.Contains(Name) || a.LastName.Contains(Name)).ToList();
        }

        public Admin GetByVToken(string VToken)
        {
            return _shopContext.Admins.Where((a) => a.VToken == VToken).FirstOrDefault();
        }

        public Admin Update(Admin OriginalAdmin,Admin UpdatedAdmin)
        {
            OriginalAdmin.LastName = UpdatedAdmin.LastName;
            OriginalAdmin.PhoneNumber = UpdatedAdmin.PhoneNumber;
            OriginalAdmin.FirstName = UpdatedAdmin.FirstName;
            OriginalAdmin.Email = UpdatedAdmin.Email;
            OriginalAdmin.Password = UpdatedAdmin.Password;
            OriginalAdmin.PasswordSalt = UpdatedAdmin.PasswordSalt;

            _shopContext.Update(OriginalAdmin);
            return OriginalAdmin;
        }
    }
}
