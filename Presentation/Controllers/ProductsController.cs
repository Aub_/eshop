﻿using Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/products")]
    public class ProductsController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public ProductsController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public ActionResult GetProducts([FromQuery] int size, string color,string tag)
        {
            var products = _serviceManager.ProductService.GetAll();
            return Ok(products);
        }

        [HttpGet("{id}")]
        public ActionResult GetProduct(int id)
        {
            var product =  _serviceManager.ProductService.GetById(id);
            return Ok(product);
        }

        [HttpGet("category/{id}")]
        public ActionResult GetByCategoryId(int id)
        {
            var productList = _serviceManager.ProductService.GetByCatId(id);
            return Ok(productList);
        }

        [HttpGet("subcategory/{id}")]
        public ActionResult GetBySubCategoryId(int id)
        {
            var productList = _serviceManager.ProductService.GetBySubCatId(id);
            return Ok(productList);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult AddProduct([FromBody] ProductForCreationDTO productForCreationDTO)//TODO make it take multiple
        {
            var createdProduct = _serviceManager.ProductService.Create(productForCreationDTO);
            return Created("",createdProduct);
        }


        [Authorize(Roles = "Admin")]
        [HttpPut]
        public ActionResult UpdateProduct([FromBody] ProductForUpdateDTO productForUpdateDTO)
        {
            var updatedProduct = _serviceManager.ProductService.Update(productForUpdateDTO);
            return Ok(updatedProduct);
        }


        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(int id)
        {
            _serviceManager.ProductService.Delete(id);
            return NoContent();
        }
    }
}
