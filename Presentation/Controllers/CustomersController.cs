﻿using Contracts;
using Contracts.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/customers")]
    public class CustomersController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;
        public CustomersController(IServiceManager serviceManager)
        { 
            _serviceManager = serviceManager;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult<List<CustomerDTO>> GetAll()
        {
            List<CustomerDTO> customers = _serviceManager.CustomerService.GetAll();
            return Ok(customers);
        }

        [Authorize(Roles = "Admin,Customer")]
        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetById(int id)
        {
            var token = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            CallerDetailsModel caller = _serviceManager.TokenHandler.GetCallerDetails(token);

            if (caller.Id != id && caller.Role == "Customer")
                return Forbid();
            CustomerDTO customer = _serviceManager.CustomerService.GetById(id);
            if (customer is null)
                return NotFound();
            return Ok(customer);

        }

        [Authorize(Roles = "Admin")]
        [HttpPost("email")]
        public ActionResult<CustomerDTO> GetByEmail(EmailModel Email)
        {
            CustomerDTO customer = _serviceManager.CustomerService.GetByEmail(Email.EmailAddress);
            if (customer is null)
                return NotFound();
            return Ok(customer);
        }

        [Authorize(Roles = "Admin, Customer")]
        [HttpPut("{id}")]
        public ActionResult<CustomerDTO> Update(int id, [FromBody] CustomerForUpdateDTO customerForUpdateDTO)
        {
            var token = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            CallerDetailsModel caller = _serviceManager.TokenHandler.GetCallerDetails(token);

            if (caller.Id != id && caller.Role == "Customer")
                return Forbid();
            CustomerDTO updatedCustomer = _serviceManager
                .CustomerService.Update(customerForUpdateDTO);
            
            if(updatedCustomer is null) return NotFound();
            return Ok(updatedCustomer);
        }

        [HttpPost("signup")]
        public ActionResult Register([FromBody] RegistrationDTO registrationDto)
        {
            CustomerDTO newCustomer = _serviceManager.CustomerService.Signup(registrationDto);
            if (newCustomer != null)
                return Ok(newCustomer);
            return Conflict("The email address already exists.");
        }

        [HttpPost("login")]
        public ActionResult CustomerLogin([FromBody] LoginDTO loginDTO)
        {
            CustomerDTO customer = _serviceManager.CustomerService.Login(loginDTO);
            if (customer is null)
                return StatusCode(403);

            string token = _serviceManager.TokenHandler.GenerateToken(customer.ID, "Customer");
            RefreshToken refreshToken = _serviceManager.CustomerService.CreateRefreshToken(customer.ID);
            JWTModel Token = new JWTModel(token, refreshToken);
            return Ok(Token);
        }

        [Authorize]
        [HttpPost("password/change")]
        public ActionResult ChangePasswordCustomer([FromBody] ChangePasswordModel changePasswordModel)
        {
            string oldPassword = changePasswordModel.OldPassword;
            string newPassword = changePasswordModel.NewPassword;
            var token = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            CallerDetailsModel caller = _serviceManager.TokenHandler.GetCallerDetails(token);

            CustomerDTO customer = _serviceManager.CustomerService.GetById(caller.Id);
            if (customer != null)
            {
                customer = _serviceManager.CustomerService.ChangePassword(newPassword, oldPassword, customer);
            }
            else return StatusCode(403);

            return Ok(customer);
        }
        [HttpPost("password/forgot")]
        public ActionResult ForgotPasswordCustomer([FromBody] EmailModel forgotPasswordModel)
        {
            string email = forgotPasswordModel.EmailAddress;
            CustomerDTO customer = _serviceManager.CustomerService.GetByEmail(email);
            if (customer is null) return NotFound();

            string changePassowrdToken = _serviceManager.CustomerService.SetPasswordResetToken(customer);
            return Ok(changePassowrdToken);//for now until I send it by email
        }

        [HttpPost("password/reset")]
        public ActionResult ResetPasswordCustomer([FromBody] ResetPasswordModel resetPasswordModel)
        {
            bool result = _serviceManager.CustomerService.ResetPassword(resetPasswordModel);
            if (result) return Ok();
            return StatusCode(403);
        }

        [HttpPost("refreshtoken")]
        public ActionResult RefreshToken([FromBody] JWTModel Tokens)
        {
            JWTModel newTokens = _serviceManager.CustomerService.RefreshToken(Tokens);
            if (newTokens is null) return Unauthorized();

            return Ok(newTokens);
        }

        [Authorize(Roles ="Admin")]
        [HttpPost("revoke/{id}")]
        public ActionResult RevokeRefreshToken(int id)
        {
            bool result = _serviceManager.CustomerService.RevokeRefreshToken(id);
            if (!result) return NotFound();

            return NoContent();
        }
    }
}
