﻿//using Contracts.Authentication;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Options;
//using Microsoft.IdentityModel.Tokens;
//using Services.Abstractions;
//using System;
//using System.IdentityModel.Tokens.Jwt;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Web.Middlewares
//{
//    public class JWTMiddleware
//    {
//        private readonly RequestDelegate _next;
//        private readonly AppSettings _appSettings;
//        private readonly IConfiguration _configuration;

//        public JWTMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings,
//            IConfiguration configuration)
//        {
//            _next = next;
//            _appSettings = appSettings.Value;
//            _configuration = configuration;
//        }

//        public async Task Invoke(HttpContext context, IServiceManager serviceManager)
//        {
//            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

//            if (token != null)
//            {
//                var jwtToken = DecodeToken(token);
//                string userRole = jwtToken.Claims.First(x => x.Type == "role").Value;
//                if (userRole == "Admin")
//                {
//                    attachAdminToContext(context, serviceManager.AdminService, jwtToken);
//                }
//                else if (userRole == "Customer")
//                {
//                    attachCustomerToContext(context, serviceManager.CustomerService, jwtToken);
//                }
//            }
                

//            await _next(context);
//        }

//        private void attachAdminToContext(HttpContext context, IAdminService AdminService, JwtSecurityToken token)
//        {
//            int adminId = int.Parse(token.Claims.First(x => x.Type == "id").Value);

//            context.Items["Admin"] = AdminService.GetById(adminId);
//        }

//        private void attachCustomerToContext(HttpContext context, ICustomerService CustomerService, JwtSecurityToken token)
//        {
//            int customerId = int.Parse(token.Claims.First(x => x.Type == "id").Value);

//            context.Items["Customer"] = CustomerService.GetById(customerId);
//        }

//        private JwtSecurityToken DecodeToken(string token)
//        {
//            var tokenHandler = new JwtSecurityTokenHandler();
//            string secret = _configuration.GetSection("AppSettings:Secret").Value.ToString();
//            var key = Encoding.ASCII.GetBytes(secret);
//            tokenHandler.ValidateToken(token, new TokenValidationParameters
//            {
//                ValidateIssuerSigningKey = true,
//                IssuerSigningKey = new SymmetricSecurityKey(key),
//                ValidateIssuer = false,
//                ValidateAudience = false,
//                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
//                ClockSkew = TimeSpan.Zero
//            }, out SecurityToken validatedToken);

//            return (JwtSecurityToken)validatedToken;
//        }
//    }


//    public class AppSettings
//    {
//        public string Secret { get; set; }
//    }
//}
