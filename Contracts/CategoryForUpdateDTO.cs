﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contracts
{
    public class CategoryForUpdateDTO
    {
        //TODO add validation decorators
        [Required]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
