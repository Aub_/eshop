﻿using Contracts;
using Contracts.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/admins")]
    public class AdminsController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public AdminsController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult<List<AdminDTO>> GetAdmins()
        {
            List<AdminDTO> Admins = _serviceManager.AdminService.GetAll();
            return Ok(Admins);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public ActionResult<List<AdminDTO>> GetAdmin(int id)
        {
            AdminDTO Admin = _serviceManager.AdminService.GetById(id);
            if (Admin is null)
                return NotFound();
            return Ok(Admin);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add([FromBody] List<AdminDTO> NewAdmins)
        {
            _serviceManager.AdminService.Add(NewAdmins);
            return Ok();
        }


        [HttpPost("login")]
        public ActionResult AdminLogin([FromBody] LoginDTO loginDTO)
        {
            AdminDTO admin = _serviceManager.AdminService.Login(loginDTO);
            if (admin is null)
                return StatusCode(403);

            string token = _serviceManager.TokenHandler.GenerateToken(admin.ID, "Admin");
            RefreshToken refreshToken = _serviceManager.AdminService.CreateRefreshToken(admin.ID);
            JWTModel Token = new JWTModel(token, refreshToken);
            return Ok(Token);
        }

        [Authorize]
        [HttpPost("password/change")]
        public ActionResult ChangePasswordAdmin([FromBody] ChangePasswordModel changePasswordModel)
        {
            string oldPassword = changePasswordModel.OldPassword;
            string newPassword = changePasswordModel.NewPassword;
            var token = HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            CallerDetailsModel caller = _serviceManager.TokenHandler.GetCallerDetails(token);

            AdminDTO admin = _serviceManager.AdminService.GetById(caller.Id);
            if (admin != null)
            {
                admin = _serviceManager.AdminService.
                    ChangePassword(newPassword, oldPassword, admin);
            }
            else return StatusCode(403);

            return Ok(admin);
        }

        [HttpPost("password/forgot")]
        public ActionResult ForgotPasswordAdmin([FromBody] EmailModel forgotPasswordModel)
        {
            string email = forgotPasswordModel.EmailAddress;
            AdminDTO admin = _serviceManager.AdminService.GetByEmail(email);
            if (admin is null) return NotFound();

            string changePassowrdToken = _serviceManager.AdminService.SetPasswordResetToken(admin);
            return Ok(changePassowrdToken);//for now until I send it by email
        }

        [HttpPost("password/reset")]
        public ActionResult ResetPasswordAdmin([FromBody] ResetPasswordModel resetPasswordModel)
        {
            bool result = _serviceManager.AdminService.ResetPassword(resetPasswordModel);
            if (result) return Ok();
            return StatusCode(403);
        }

        [HttpPost("refreshtoken")]
        public ActionResult RefreshToken([FromBody] JWTModel Tokens)
        {
            JWTModel newTokens = _serviceManager.AdminService.RefreshToken(Tokens);
            if (newTokens is null) return Unauthorized();

            return Ok(newTokens);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("revoke/{id}")]
        public ActionResult RevokeRefreshToken(int id)
        {
            bool result = _serviceManager.AdminService.RevokeRefreshToken(id);
            if (!result) return NotFound();

            return NoContent();
        }
    }
}
