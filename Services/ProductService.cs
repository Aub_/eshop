﻿using Contracts;
using Domain.Repositories;
using Services.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Exceptions;
using Domain.Entities;
using Mapster;

namespace Services
{
    public sealed class ProductService : IProductService
    {
        private readonly IRepositoryManager _repositoryManager;

        public ProductService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public ProductDTO Create(ProductForCreationDTO productForCreationDTO)
        {
            var product = productForCreationDTO.Adapt<Product>();

            _repositoryManager.ProductRepository.Add(product);

            _repositoryManager.UnitOfWork.SaveChanges();

            return product.Adapt<ProductDTO>();
        }

        public void Delete(int id)
        {
            Product product = _repositoryManager.ProductRepository.GetById(id);

            if(product is null)
            {
                throw new NotFoundException(id);
            }
            _repositoryManager.ProductRepository.Remove(product);
            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public List<ProductDTO> GetAll()
        {
            List<Product> products =  _repositoryManager.ProductRepository.GetAll();
            return products.Adapt<List<ProductDTO>>();
        }

        public List<ProductDTO> GetByCatId(int catId)
        {
            Category category = _repositoryManager.CategoryRepository.GetById(catId);
            if(category is null)
            {
                throw new NotFoundException(catId);
            }
            var productList = _repositoryManager.ProductRepository.GetByCategoryId(catId);
            return productList.Adapt<List<ProductDTO>>();
        }

        public ProductDTO GetById(int id)
        {
            Product product = _repositoryManager.ProductRepository.GetById(id);

            if (product is null)
            {
                throw new NotFoundException(id);
            }

            return product.Adapt<ProductDTO>();
        }

        public List<ProductDTO> GetBySubCatId(int subCatId)
        {
            SubCategory subCategory = _repositoryManager.SubCategoryRepository.GetById(subCatId);
            if(subCategory is null)
            {
                throw new NotFoundException(subCatId);
            }

            List<Product> ProductList = _repositoryManager.ProductRepository.GetBySubCategoryId(subCatId);
            return ProductList.Adapt<List<ProductDTO>>();
        }

        public ProductDTO Update(ProductForUpdateDTO productForUpdateDTO)//TODO fix, this does not even call repository
        {
            int id = productForUpdateDTO.Id;
            Product product = _repositoryManager.ProductRepository.GetById(id);

            if (product is null)
            {
                throw new NotFoundException(id);
            }

            product.Name = productForUpdateDTO.Name;
            product.PriceEU = productForUpdateDTO.PriceEU;
            product.PriceUSD = productForUpdateDTO.PriceUSD;
            product.Size = productForUpdateDTO.Size;
            product.Stars = productForUpdateDTO.Stars;

            _repositoryManager.UnitOfWork.SaveChanges();
            return product.Adapt<ProductDTO>();
        }
    }
}
