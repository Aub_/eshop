﻿using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Mapster;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    class CategoryService : ICategoryService
    {
        private readonly IRepositoryManager _repositoryManager;

        public CategoryService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public void Add (List<CategoryForCreationDTO> categories)
        {
            var categoriesToAdd = categories.Adapt<List<Category>>();
            _repositoryManager.CategoryRepository.Add(categoriesToAdd);

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public void Delete(List<int> categoryIds)
        {
            List<Category> categoriesToDelete = _repositoryManager.CategoryRepository.GetByIds(categoryIds);
            if(categoriesToDelete.Count != 0 || categoriesToDelete != null)
                _repositoryManager.CategoryRepository.Delete(categoriesToDelete);

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public List<CategoryDTO> GetAll()
        {
            var categories = _repositoryManager.CategoryRepository.GetAll();
            return categories.Adapt<List<CategoryDTO>>();
        }

        public CategoryDTO GetById(int categoryId)
        {
            Category category = _repositoryManager.CategoryRepository.GetById(categoryId);
            if(category == null) return null;

            return category.Adapt<CategoryDTO>();
        }

        public List<CategoryDTO> GetByIds(List<int> categoryIds)
        {
            List<Category> categories = _repositoryManager.CategoryRepository.GetByIds(categoryIds);
            return categories.Adapt<List<CategoryDTO>>();
        }

        public List<CategoryDTO> GetByName(string name)
        {
            List<Category> categories = _repositoryManager.CategoryRepository.GetByName(name);
            return categories.Adapt<List<CategoryDTO>>();
        }

        public CategoryDTO Update(CategoryForUpdateDTO category)
        {
            Category original =  _repositoryManager.CategoryRepository.GetById(category.ID);
            if (original == null) return null;

            original = _repositoryManager.CategoryRepository.Update(original, category.Adapt<Category>());

            _repositoryManager.UnitOfWork.SaveChanges();
            return original.Adapt<CategoryDTO>();
        }
    }
}
