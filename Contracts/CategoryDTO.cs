﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public class CategoryDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
