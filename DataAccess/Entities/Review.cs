﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Review
    {
        [Key]
        public int ID { get; set; }
        public int AuthorId { get; set; }
        public int Stars { get; set; }
        public string ReviewText { get; set; }
        public int HelpfulCount { get; set; }
        public int NotHelpfulCount { get; set; }
        public int ProductId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public List<ReviewPhoto> ReviewPhotos { get; set; }
        public Product Product { get; set; }
    }
}
