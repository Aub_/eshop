﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts;

namespace Services.Abstractions
{
    public interface IProductService
    {
        List<ProductDTO> GetAll();
        ProductDTO GetById(int id);
        List<ProductDTO> GetByCatId(int catId);
        List<ProductDTO> GetBySubCatId(int subCatId);
        ProductDTO Create(ProductForCreationDTO productForCreationDTO);
        ProductDTO Update(ProductForUpdateDTO productForUpdateDTO);
        void Delete(int id);
    }
}
