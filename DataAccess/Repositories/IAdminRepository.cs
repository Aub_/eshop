﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IAdminRepository
    {
        List<Admin> GetAll();
        Admin GetById(int Id);
        List<Admin> GetByIds(List<int> Ids);
        List<Admin> GetByName(string Name);
        Admin GetByEmail(string Email);
        bool Activate(Admin Admin);
        void Add(List<Admin> NewAdmins);
        Admin Update(Admin OriginalAdmin , Admin UpdatedAdmin);
        Admin GetByVToken(string VToken);
        void Delete(List<Admin> DeletedAdmins);
    }
}
