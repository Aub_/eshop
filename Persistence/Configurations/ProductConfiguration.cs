﻿using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    public sealed class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(nameof(Product));
            builder.HasKey(product => product.Id);
            builder.Property(product => product.Name).IsRequired();
            builder.Property(product => product.Name).HasMaxLength(60);
            builder.Property(product => product.Color).IsRequired();
            builder.Property(product => product.Description).HasMaxLength(300);
            builder.Property(product => product.CreatedAt).HasDefaultValue(DateTime.UtcNow);
            builder.Property(product => product.UpdatedAt).HasDefaultValue(DateTime.UtcNow);
            builder.Property(product => product.Quantity).IsRequired();
            builder.HasMany(product => product.ProductPhotos)
                .WithOne()
                .HasForeignKey(productPhoto => productPhoto.ProdId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
