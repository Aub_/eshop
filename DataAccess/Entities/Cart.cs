﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Cart
    {
        [Key]
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public int Total { get; set; }
        public int DiscountId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Customer Customer { get; set; }
        public DiscountCode DiscountCode { get; set; }

    }
}
