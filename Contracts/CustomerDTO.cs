﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Contracts
{
    public class CustomerDTO
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public string Gender { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        // Account Details

        public string EmailAddress { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }
        public string PhoneNumber { get; set; }
    }
}
