﻿using Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/categories")]
    public class CategoriesController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public CategoriesController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public ActionResult<List<CategoryDTO>> GetAll()
        {
            List<CategoryDTO> categories = _serviceManager.CategoryService.GetAll();
            return Ok(categories);
        }

        [HttpGet("name/{name}")]
        public ActionResult<List<CategoryDTO>> GetByName(string name)
        {
            List<CategoryDTO> categories = _serviceManager.CategoryService.GetByName(name);
            return Ok(categories);
        }

        [HttpGet("{id}")]
        public ActionResult<CategoryDTO> GetById(int id)
        {

            CategoryDTO category = _serviceManager.CategoryService.GetById(id);
            if (category == null)
                return NotFound();

            return Ok(category);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add(List<CategoryForCreationDTO> newCategories)
        {
            _serviceManager.CategoryService.Add(newCategories);
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public ActionResult Update(CategoryForUpdateDTO updatedCategory)
        {
            CategoryDTO category = _serviceManager.CategoryService.Update(updatedCategory);

            if (category == null)
                return NotFound();

            return Ok(category);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("delete")]
        public ActionResult Delete([FromBody] List<int> ids)
        {
            _serviceManager.CategoryService.Delete(ids);
            return Ok();
        }

    }
}
