﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class CustomerAddress
    {
        //[Key]
        public int AddressId { get; set; }
        public int CustomerId { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Address Address { get; set; }
        public Customer Customer { get; set; }
    }
}
