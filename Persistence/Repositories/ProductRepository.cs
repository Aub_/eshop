﻿using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public sealed class ProductRepository : IProductRepository
    {
        private readonly ShopContext _shopContext;

        public ProductRepository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public void Add(Product product)
        {
            _shopContext.Products.Add(product);
        }

        public List<Product> GetAll()
        {
            return _shopContext.Products.Include(p=>p.ProductPhotos).ToList();
        }

        public List<Product> GetByCategoryId(int catId)
        {
            var ProductList = _shopContext.Products.Where(p => p.SubCategory.CategoryID == catId)
                .Include(p => p.SubCategory)
                .Include(p => p.ProductPhotos)
                .ToList();
            return ProductList;
        }

        public Product GetById(int id)
        {
            return _shopContext.Products.Include(p => p.ProductPhotos).Where(p => p.Id == id).FirstOrDefault();
        }

        public List<Product> GetBySubCategoryId(int subCatId)
        {
            return _shopContext.Products.Include(p => p.ProductPhotos).Where(p => p.SubCatId == subCatId).ToList();
        }

        public void Remove(Product product)
        {
            _shopContext.Remove(product);
        }
    }
}
