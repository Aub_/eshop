﻿using Contracts;
using Domain.Entities;
using Domain.Repositories;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    class CustomerRepository : ICustomerRepository
    {
        private readonly ShopContext _shopContext;

        public CustomerRepository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public void AddCustomer(Customer customer)
        {
            _shopContext.Customers.Add(customer);
        }

        public bool EmailExists(string emailAddress)
        {
            Customer customer = _shopContext.Customers.Where(c => c.EmailAddress.Equals(emailAddress)).FirstOrDefault();
            return customer != null;
        }

        public List<Customer> GetAll()
        {
            return _shopContext.Customers.ToList();
        }

        public Customer GetByEmail(string emailAddress)
        {
           return _shopContext.Customers.Where(c => c.EmailAddress.Equals(emailAddress)).FirstOrDefault();
        }

        public Customer GetById(int id)
        {
            return _shopContext.Customers.Where(c => c.ID == id).FirstOrDefault();
        }

        public Customer GetByVtoken(string vtoken)
        {
            return _shopContext.Customers.Where(c=>c.VToken == vtoken).FirstOrDefault();
        }

        public Customer Update(Customer originalCustomer, Customer updatedCustomer)
        {
            originalCustomer.DoB = updatedCustomer.DoB;
            originalCustomer.Gender = updatedCustomer.Gender;
            originalCustomer.FirstName = updatedCustomer.FirstName;
            originalCustomer.LastName = updatedCustomer.LastName;
            originalCustomer.VToken = updatedCustomer.VToken;
            originalCustomer.PasswordSalt = updatedCustomer.PasswordSalt;
            originalCustomer.PhoneNumber = updatedCustomer.PhoneNumber;

            _shopContext.Update(originalCustomer);
            return originalCustomer;
        }
    }
}
