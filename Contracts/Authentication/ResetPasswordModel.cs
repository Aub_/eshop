﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Authentication
{
    public class ResetPasswordModel
    {
        public string NewPassword { get; set; }
        public string PasswordResetToken { get; set; }
    }
}
