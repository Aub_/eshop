﻿using Contracts;
using Contracts.Authentication;
using Domain.Entities;
using Domain.Repositories;
using Mapster;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    class AdminService : IAdminService
    {
        #region privates 
        private readonly IRepositoryManager _repositoryManager;
        private readonly IPasswordService _passwordService;
        private readonly ITokenHandler _tokenHandler;
        public AdminService(IRepositoryManager repositoryManager, IPasswordService passwordService, ITokenHandler tokenHandler)
        {
            _repositoryManager = repositoryManager;
            _passwordService = passwordService;
            _tokenHandler = tokenHandler;
        }
        #endregion
        public void Add(List<AdminDTO> NewAdmins)
        {
            List<Admin> NewAdminsDBList = NewAdmins.Adapt<List<Admin>>();
            
            foreach(Admin admin in NewAdminsDBList)
            {
                admin.Password = _passwordService.HashPassword(admin.Password, out byte[] Salt);
                admin.PasswordSalt = Salt;
            }

            _repositoryManager.AdminRepository.Add(NewAdminsDBList);

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public void Delete(List<int> DeletedAdminIds)
        {
            List<Admin> adminsToDelete = _repositoryManager.AdminRepository.GetByIds(DeletedAdminIds);

            if (adminsToDelete.Count != 0 || adminsToDelete != null)
                _repositoryManager.AdminRepository.Delete(adminsToDelete);

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public List<AdminDTO> GetAll()
        {
            List<Admin> Admins = _repositoryManager.AdminRepository.GetAll();
            return Admins.Adapt<List<AdminDTO>>();
        }

        public AdminDTO GetByEmail(string Email)
        {
            Admin admin = _repositoryManager.AdminRepository.GetByEmail(Email);
            if (admin is null) return null;
            return admin.Adapt<AdminDTO>();
        }

        public AdminDTO GetById(int AdminId)
        {
            Admin Admin = _repositoryManager.AdminRepository.GetById(AdminId);
            if (Admin is null)
                return null;
            return Admin.Adapt<AdminDTO>();
        }

        public AdminDTO Update(AdminDTO UpdatedAdmin)
        {
            Admin originalAdmin = _repositoryManager.AdminRepository.GetById(UpdatedAdmin.ID);
            if (originalAdmin is null)
                return null;
            originalAdmin = _repositoryManager.AdminRepository.Update(originalAdmin, UpdatedAdmin.Adapt<Admin>());
            return originalAdmin.Adapt<AdminDTO>();
        }


        #region account-related methods
        public AdminDTO Login(LoginDTO loginDTO)
        {
            Admin admin = _repositoryManager.AdminRepository.GetByEmail(loginDTO.Email);
            if(admin is null) return null;

            bool password_is_correct = _passwordService
                .ComparePasswords(loginDTO.Password, admin.Password, admin.PasswordSalt);

            if (password_is_correct)
                return admin.Adapt<AdminDTO>();

            return null;
        }

        public bool ResetPassword(ResetPasswordModel model)
        {
            string resetToken = model.PasswordResetToken;
            string newPassword = model.NewPassword;

            Admin admin = _repositoryManager.AdminRepository.GetByVToken(resetToken);
            if (admin is null) return false;

            if (admin.VTokenExpiry < DateTime.Now) return false;

            string passwordHash = _passwordService.HashPassword(newPassword, out byte[] passwordSalt);
            admin.VTokenExpiry = DateTime.MinValue;
            admin.Password = passwordHash;
            admin.PasswordSalt = passwordSalt;

            _repositoryManager.UnitOfWork.SaveChanges();
            return true;
        }

        public string SetPasswordResetToken(AdminDTO admin)
        {
            Admin dbModel = _repositoryManager.AdminRepository.GetById(admin.ID);
            if (dbModel is null) return null;

            Random rand = new Random();
            string changePasswordToken = rand.Next(99999999, 999999999).ToString();

            dbModel.VToken = changePasswordToken;
            dbModel.VTokenExpiry = DateTime.Now.AddMinutes(15);

            _repositoryManager.UnitOfWork.SaveChanges();
            return changePasswordToken;
        }

        public AdminDTO ChangePassword(string newPassword, string oldPassword, AdminDTO admin)
        {
            bool password_is_correct = _passwordService
                   .ComparePasswords(oldPassword, admin.Password, admin.PasswordSalt);
            if (password_is_correct)
            {
                string new_password_hash = _passwordService.HashPassword(newPassword, out byte[] salt);
                admin.PasswordSalt = salt;
                admin.Password = new_password_hash;

                _repositoryManager.UnitOfWork.SaveChanges();
                return admin;
            }
            else return null;
        }

        #endregion

        #region refresh token related methods
        public RefreshToken CreateRefreshToken(int id)
        {
            Admin admin = _repositoryManager.AdminRepository.GetById(id);
            if (admin == null)
                return null;

            var randomNumber = new byte[64];
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(randomNumber);
            string refreshToken = Convert.ToBase64String(randomNumber);
            DateTime refreshTokenExpiry = DateTime.Now.AddDays(30);
            admin.RefreshToken = refreshToken;
            admin.RefreshTokenExpiry = refreshTokenExpiry;

            _repositoryManager.UnitOfWork.SaveChanges();
            return new RefreshToken(refreshToken, refreshTokenExpiry);
        }

        public JWTModel RefreshToken(JWTModel oldTokens)
        {
            string old_access_token = oldTokens.AccessToken;
            string old_refresh_token = oldTokens.RefreshToken.Token;

            ClaimsPrincipal? principal = _tokenHandler.GetPrincipalFromExpiredToken(old_access_token);
            if (principal is null) return null;

            int admin_id = Int32.Parse(principal.Claims.First(x => x.Type == "id").Value);


            Admin admin = _repositoryManager.AdminRepository.GetById(admin_id);
            if (admin is null || admin.RefreshToken != old_refresh_token || admin.RefreshTokenExpiry < DateTime.Now)
                return null;

            //everything is fine, let's create the tokens
            string new_access_token = _tokenHandler.GenerateToken(admin_id, "Admin");
            RefreshToken new_refresh_token = CreateRefreshToken(admin_id);

            JWTModel Token = new JWTModel(new_access_token, new_refresh_token);
            return Token;
        }

        public bool RevokeRefreshToken(int id)
        {
            Admin admin = _repositoryManager.AdminRepository.GetById(id);
            if (admin is null) return false;

            admin.RefreshToken = null;
            admin.RefreshTokenExpiry = DateTime.MinValue;

            _repositoryManager.UnitOfWork.SaveChanges();
            return true;
        }
        #endregion
    }
}
