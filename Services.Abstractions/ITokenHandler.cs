﻿using Contracts.Authentication;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Services.Abstractions
{
    public interface ITokenHandler
    {
        public string GenerateToken(int user_id, string role);
        public CallerDetailsModel GetCallerDetails(string token);
        public ClaimsPrincipal? GetPrincipalFromExpiredToken(string token);
    }
}
