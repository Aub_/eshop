﻿using Contracts;
using Domain.Entities;
using Domain.Exceptions;
using Domain.Repositories;
using Mapster;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SubCategoryService : ISubCategoryService
    {
        private readonly IRepositoryManager _repositoryManager;

        public SubCategoryService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public void Add(List<SubCategoryForCreationDTO> newSubCategories)
        {
            var subCategoriesToAdd = newSubCategories.Adapt<List<SubCategory>>();
            _repositoryManager.SubCategoryRepository.Add(subCategoriesToAdd);

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public void Delete(List<int> ToBeDeletedSubCategories)
        {
            List<SubCategory> subCategoriesToDelete = _repositoryManager.SubCategoryRepository.GetByIds(ToBeDeletedSubCategories);

            if (subCategoriesToDelete.Count != 0 || subCategoriesToDelete != null)
            {
                _repositoryManager.SubCategoryRepository.Delete(subCategoriesToDelete);
            }

            _repositoryManager.UnitOfWork.SaveChanges();
        }

        public List<SubCategoryDTO> GetAll()
        {
            var SubCategories = _repositoryManager.SubCategoryRepository.GetAll();
            if (SubCategories != null)
            {
                var x = SubCategories.Adapt<List<SubCategoryDTO>>();
                return x;
            }
            return null;
        }

        public List<SubCategoryDTO> GetByCategoryId(int categoryId)
        {
            List<SubCategory> SubCategories = _repositoryManager.SubCategoryRepository.GetByCategoryId(categoryId);
            if (SubCategories.Count != 0)
                return SubCategories.Adapt<List<SubCategoryDTO>>();
            return null;
        }

        public SubCategoryDTO GetById(int Id)
        {
            SubCategory SubCategory = _repositoryManager.SubCategoryRepository.GetById(Id);
            if (SubCategory != null)
                return SubCategory.Adapt<SubCategoryDTO>();
            return null;
        }

        public SubCategoryDTO Update(SubCategoryForUpdateDTO updatedCategory)
        {
            var SubCategoryToBeUpdated = updatedCategory.Adapt<SubCategory>();
            SubCategory original = _repositoryManager.SubCategoryRepository.GetById(updatedCategory.ID);
            var updated = _repositoryManager.SubCategoryRepository.Update(original,SubCategoryToBeUpdated);

            _repositoryManager.UnitOfWork.SaveChanges();
            return updated.Adapt<SubCategoryDTO>();
        }
    }
}
