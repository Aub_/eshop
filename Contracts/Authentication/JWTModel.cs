﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Contracts.Authentication
{
    public class JWTModel
    {
        public JWTModel()
        {
        }

        public JWTModel(string token)
        {
            AccessToken = token;
        }

        public JWTModel(string token, RefreshToken refreshToken) : this(token)
        {
            RefreshToken = refreshToken;
        }

        [Required]
        public string AccessToken { get; set; }
        [Required]
        public RefreshToken RefreshToken { get; set; }
    }

    public class RefreshToken
    {
        public RefreshToken(string refreshToken, DateTime refreshTokenExpiry)
        {
            Token = refreshToken;
            ExpiryDate = refreshTokenExpiry;
        }
        public RefreshToken()
        {

        }
        public string Token { get; set; }
        [JsonIgnore]
        public DateTime ExpiryDate { get; set; }
    }
}
