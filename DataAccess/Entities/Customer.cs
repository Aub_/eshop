﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Customer// : Account
    {
        [Key]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DoB { get; set; }
        public string Gender { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        // Account Details

        public string EmailAddress { get; set; }
        public string VToken { get; set; }
        public DateTime VTokenExpiry { get; set; }
        public string PhoneNumber { get; set; }

        public Byte[] PasswordSalt { get; set; }
        public string PasswordHash { get; set; }

        public string RefreshToken { get; set; }
        public DateTime RefreshTokenExpiry { get; set; }

    }
}
