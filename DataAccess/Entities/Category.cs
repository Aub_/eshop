﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Category
    {
        public Category()
        {
            SubCats = new List<SubCategory>();
        }

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public List<SubCategory> SubCats { get; set; }
        public List<Seller> Sellers { get; set; }
    }
}
