﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Seller //: Account
    {
        public Seller()
        {
            Products = new List<Product>();
        }

        [Key]
        public int ID { get; set; }
        public int CatId { get; set; }
        public string Name { get; set; }
        public int AddressId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }


        //acccount details
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }

        public Byte[] PasswordSalt { get; set; }
        public string PasswordHash { get; set; }

        public Category Category { get; set; }
        public List<Product> Products { get; set; }
        public Address Address { get; set; }
       

    }
}
