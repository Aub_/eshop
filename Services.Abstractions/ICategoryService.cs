﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface ICategoryService
    {
        void Add(List<CategoryForCreationDTO> categories);
        void Delete(List<int> categoryIds);
        CategoryDTO GetById(int categoryId);
        List<CategoryDTO> GetByIds(List<int> categoryIds);
        List<CategoryDTO> GetByName(string name);
        List<CategoryDTO> GetAll();
        // TODO change later to take many at once
        CategoryDTO Update(CategoryForUpdateDTO category);
    }
}
