﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface IPasswordService
    {
        string HashPassword(string Password, out Byte[] PasswordSalt);
        bool ComparePasswords(string PasswordOne, string PasswordTwo, Byte[] PasswordTwoSalt);
    }
}
