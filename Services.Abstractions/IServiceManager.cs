﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface IServiceManager
    {
        IProductService ProductService { get; }
        ICategoryService CategoryService { get; }
        ISubCategoryService SubCategoryService { get; }
        IAdminService AdminService { get; }
        ICustomerService CustomerService { get; }
        IPasswordService PasswordService { get; }
        ITokenHandler TokenHandler { get; }
    }
}
