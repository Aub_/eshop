﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories
{
    public interface IRepositoryManager
    {
        IProductRepository ProductRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        ISubCategoryRepository SubCategoryRepository { get;}
        IAdminRepository AdminRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IUnitOfWork UnitOfWork { get; }
    }
}
