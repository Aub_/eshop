﻿using Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [ApiController]
    [Route("api/subcategories")]
    public class SubCategoriesController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public SubCategoriesController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public ActionResult<List<SubCategoryDTO>> GetAll()
        {
            List<SubCategoryDTO> SubCatList = _serviceManager.SubCategoryService.GetAll();
            return Ok(SubCatList);
        }

        [HttpGet("{id}")]
        public ActionResult<SubCategoryDTO> GetById(int id)
        {
            SubCategoryDTO SubCat = _serviceManager.SubCategoryService.GetById(id);
            if(SubCat != null)
                return Ok(SubCat);
            return NotFound();
        }

        [HttpGet("category/{id}")]
        public ActionResult<List<SubCategoryDTO>> GetByCategoryId(int id)
        {
            List<SubCategoryDTO> SubCatList = _serviceManager.SubCategoryService.GetByCategoryId(id);
            if (SubCatList != null)
                return Ok(SubCatList);
            return NotFound();
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Add([FromBody] List<SubCategoryForCreationDTO> NewSubCategories)
        {
            _serviceManager.SubCategoryService.Add(NewSubCategories);
            return Created("",null);
        }


        [Authorize(Roles = "Admin")]
        [HttpPut]
        public ActionResult Update ([FromBody] SubCategoryForUpdateDTO SubCategory)
        {
            var Updated = _serviceManager.SubCategoryService.Update(SubCategory);
            return Ok(Updated);
        }


        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public ActionResult Delete([FromBody] List<int> IdList)
        {
            _serviceManager.SubCategoryService.Delete(IdList);
            return Ok();
        }
    }
}
