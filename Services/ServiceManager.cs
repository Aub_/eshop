﻿using System;
using System.Collections.Generic;
using System.Text;
using Services.Abstractions;
using Domain.Repositories;
using Microsoft.Extensions.Options;

namespace Services
{
    public sealed class ServiceManager : IServiceManager
    {
        private readonly Lazy<IProductService> _lazyProductService;
        private readonly Lazy<ICategoryService> _lazyCategoryService;
        private readonly Lazy<ISubCategoryService> _lazySubCategoryService;
        private readonly Lazy<IAdminService> _lazyAdminService;
        private readonly Lazy<IPasswordService> _lazyPasswordService;
        private readonly Lazy<ICustomerService> _lazyCustomerService;
        private readonly Lazy<ITokenHandler> _lazytokenHandler;
        public ServiceManager(IRepositoryManager repositoryManager)
        {
            _lazyProductService = new Lazy<IProductService>(() => new ProductService(repositoryManager));
            _lazyCategoryService = new Lazy<ICategoryService>(() => new CategoryService(repositoryManager));
            _lazySubCategoryService = new Lazy<ISubCategoryService>(()=> new SubCategoryService(repositoryManager));
            _lazyAdminService = new Lazy<IAdminService>(() => new AdminService(repositoryManager,PasswordService,TokenHandler));
            _lazyPasswordService = new Lazy<IPasswordService>(() => new PasswordService());
            _lazyCustomerService = new Lazy<ICustomerService>(() => new CustomerService(repositoryManager,PasswordService,TokenHandler));
            _lazytokenHandler = new Lazy<ITokenHandler>(() => new TokenHandler());
        }

        public IProductService ProductService => _lazyProductService.Value;
        public ICategoryService CategoryService => _lazyCategoryService.Value;
        public ISubCategoryService SubCategoryService => _lazySubCategoryService.Value;
        public IAdminService AdminService => _lazyAdminService.Value;
        public IPasswordService PasswordService => _lazyPasswordService.Value;
        public ICustomerService CustomerService => _lazyCustomerService.Value;
        public ITokenHandler TokenHandler => _lazytokenHandler.Value;
    }
}
