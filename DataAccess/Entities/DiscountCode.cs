﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class DiscountCode
    {
        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public DateTime Expiry { get; set; }
        public bool IsActive { get; set; }
        public int DiscPrecent { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
    }
}
