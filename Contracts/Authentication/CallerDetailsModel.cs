﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Authentication
{
    public class CallerDetailsModel
    {
        public CallerDetailsModel(int id, string role)
        {
            Id = id;
            Role = role;
        }

        public int Id { get; set; }
        public string Role { get; set; }
    }
}
