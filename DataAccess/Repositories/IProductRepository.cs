﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IProductRepository 
    {
        List<Product> GetAll();
        Product GetById(int id);
        List<Product> GetByCategoryId(int catId);
        List<Product> GetBySubCategoryId(int subCatId);
        void Add(Product product);
        void Remove(Product product);
    }
}
