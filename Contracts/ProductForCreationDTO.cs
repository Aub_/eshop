﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contracts
{
    public class ProductForCreationDTO
    {
        [Required]
        public int SubCatId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public double PriceUSD { get; set; }
        public double PriceEU { get; set; }
        public string Color { get; set; }
        [Required]
        public string DeliveryEstimate { get; set; }
        public double Stars { get; set; }
        public double Size { get; set; }
        [Required]
        public int Quantity { get; set; }

        public int DiscountId { get; set; }
    }
}
