﻿using Contracts;
using Contracts.Authentication;
using Domain.Entities;
using Domain.Repositories;
using Mapster;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CustomerService : ICustomerService
    {
        #region privates
        private readonly IRepositoryManager _repositoryManager;
        private readonly IPasswordService _passwordService;
        private readonly ITokenHandler _tokenHandler;
        public CustomerService(IRepositoryManager repositoryManager, IPasswordService passwordService, ITokenHandler tokenHandler)
        {
            _repositoryManager = repositoryManager;
            _passwordService = passwordService;
            _tokenHandler = tokenHandler;
        }

        #endregion

        public List<CustomerDTO> GetAll()
        {
            List<Customer> dbModels = _repositoryManager.CustomerRepository.GetAll();
            if (dbModels == null) return null;

            return dbModels.Adapt<List<CustomerDTO>>();
        }

        public CustomerDTO GetByEmail(string email)
        {
            Customer customer = _repositoryManager.CustomerRepository.GetByEmail(email);
            if (customer is null)
                return null;
            return customer.Adapt<CustomerDTO>();
        }

        public CustomerDTO GetById(int id)
        {
            Customer customer = _repositoryManager.CustomerRepository.GetById(id);
            return customer.Adapt<CustomerDTO>();
        }


        public CustomerDTO Update(CustomerForUpdateDTO updatedCustomer)
        {
            Customer original = _repositoryManager.CustomerRepository.GetById(updatedCustomer.ID);
            if (original is null)
                return null;
            CustomerDTO customerDTO = updatedCustomer.Adapt<CustomerDTO>();
            original = _repositoryManager.CustomerRepository.Update(original, updatedCustomer.Adapt<Customer>());
            _repositoryManager.UnitOfWork.SaveChanges();
            return original.Adapt<CustomerDTO>();
        }
        #region account-related methods
        public CustomerDTO ChangePassword(string newPassword, string oldPassword, CustomerDTO customer)
        {
            bool password_is_correct = _passwordService
                    .ComparePasswords(oldPassword, customer.Password, customer.PasswordSalt);
            if (password_is_correct)
            {
                string new_password_hash = _passwordService.HashPassword(newPassword, out byte[] salt);
                customer.PasswordSalt = salt;
                customer.Password = new_password_hash;

                _repositoryManager.UnitOfWork.SaveChanges();
                return customer;
            }
            else return null;
        }

        public string SetPasswordResetToken(CustomerDTO customer)
        {
            Customer dbModel = _repositoryManager.CustomerRepository.GetById(customer.ID);
            if (dbModel is null) return null;

            Random rand = new Random();
            string changePasswordToken = rand.Next(99999999, 999999999).ToString();

            dbModel.VToken = changePasswordToken;
            dbModel.VTokenExpiry = DateTime.Now.AddMinutes(15);

            _repositoryManager.UnitOfWork.SaveChanges();
            return changePasswordToken;
        }

        public CustomerDTO Signup(RegistrationDTO registrationDTO)
        {
            bool duplicateEmailCheck = _repositoryManager.CustomerRepository.EmailExists(registrationDTO.EmailAddress);
            if (duplicateEmailCheck)
                return null;//null means that the email already exists.

            Customer newCustomer = new Customer();
            newCustomer.EmailAddress = registrationDTO.EmailAddress;
            newCustomer.PhoneNumber = registrationDTO.PhoneNumber;
            newCustomer.VToken = _passwordService.HashPassword(registrationDTO.Password, out Byte[] PasswordSalt);
            newCustomer.PasswordSalt = PasswordSalt;

            _repositoryManager.CustomerRepository.AddCustomer(newCustomer);
            _repositoryManager.UnitOfWork.SaveChanges();
            return newCustomer.Adapt<CustomerDTO>();
        }

        public CustomerDTO Login(LoginDTO loginDTO)
        {
            Customer customer = _repositoryManager.CustomerRepository.GetByEmail(loginDTO.Email);
            if (customer is null)
                return null;
            bool passowrd_is_correct = _passwordService
                .ComparePasswords(loginDTO.Password, customer.PasswordHash, customer.PasswordSalt);
            if (!passowrd_is_correct)
                return null;
            return customer.Adapt<CustomerDTO>();

        }


        public bool ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            string resetToken = resetPasswordModel.PasswordResetToken;
            string newPassword = resetPasswordModel.NewPassword;

            Customer customer = _repositoryManager.CustomerRepository.GetByVtoken(resetToken);
            if (customer is null) return false;

            if (customer.VTokenExpiry < DateTime.Now) return false;

            string passwordHash = _passwordService.HashPassword(newPassword, out byte[] passwordSalt);
            customer.VTokenExpiry = DateTime.MinValue;
            customer.PasswordHash = passwordHash;
            customer.PasswordSalt = passwordSalt;

            _repositoryManager.UnitOfWork.SaveChanges();
            return true;
        }

        #endregion

        #region refresh token related methods
        public RefreshToken CreateRefreshToken(int id)
        {
            Customer customer = _repositoryManager.CustomerRepository.GetById(id);
            if (customer == null)
                return null;

            var randomNumber = new byte[64];
            using var randomNumberGenerator = RandomNumberGenerator.Create();
            randomNumberGenerator.GetBytes(randomNumber);
            string refreshToken = Convert.ToBase64String(randomNumber);
            DateTime refreshTokenExpiry = DateTime.Now.AddDays(30);
            customer.RefreshToken = refreshToken;
            customer.RefreshTokenExpiry = refreshTokenExpiry;

            _repositoryManager.UnitOfWork.SaveChanges();
            return new RefreshToken(refreshToken, refreshTokenExpiry);
        }

        public JWTModel RefreshToken(JWTModel oldTokens)
        {
            string old_access_token = oldTokens.AccessToken;
            string old_refresh_token = oldTokens.RefreshToken.Token;


            ClaimsPrincipal? principal = _tokenHandler.GetPrincipalFromExpiredToken(old_access_token);
            if (principal is null) return null;

            int customer_id = Int32.Parse(principal.Claims.First(x => x.Type == "id").Value);


            Customer customer = _repositoryManager.CustomerRepository.GetById(customer_id);
            if (customer is null || customer.RefreshToken != old_refresh_token || customer.RefreshTokenExpiry < DateTime.Now)
                return null;

            //everything is fine, let's create the tokens
            string new_access_token = _tokenHandler.GenerateToken(customer_id, "Customer");
            RefreshToken new_refresh_token = CreateRefreshToken(customer_id);

            JWTModel Token = new JWTModel(new_access_token, new_refresh_token);
            return Token;
        }

        public bool RevokeRefreshToken(int id)
        {
            Customer customer = _repositoryManager.CustomerRepository.GetById(id);
            if (customer is null) return false;

            customer.RefreshToken = null;
            customer.RefreshTokenExpiry = DateTime.MinValue;

            _repositoryManager.UnitOfWork.SaveChanges();
            return true;
        }
        #endregion
    }
}
