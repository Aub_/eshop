﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Entities
{
    public class Product
    {
        public Product()
        {
            ProductPhotos = new List<ProductPhoto>();
        }

        [Key]
        public int Id { get; set; }
        public int SubCatId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double PriceUSD { get; set; }
        public double PriceEU { get; set; }
        public string Color { get; set; }
        public string DeliveryEstimate { get; set; }
        public double Stars { get; set; }
        public double Size { get; set; }
        public int Quantity { get; set; }
        public int DiscountId { get; set; }
        public int WeightInGrams { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public List<ProductPhoto> ProductPhotos { get; set; }
        public SubCategory SubCategory { get; set; }
        public Seller Seller { get; set; }
        public DiscountCode DiscountCode { get; set; }
        public List<ProductTags> ProductTags { get; set; }

    }
}
