﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface ISubCategoryService
    {
        List<SubCategoryDTO> GetAll();
        List<SubCategoryDTO> GetByCategoryId(int categoryId);
        SubCategoryDTO GetById(int Id);
        void Add(List<SubCategoryForCreationDTO> newCategories);
        SubCategoryDTO Update(SubCategoryForUpdateDTO updatedCategory);
        void Delete(List<int> ToBeDeletedCategories);
    }
}
