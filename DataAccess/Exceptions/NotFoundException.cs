﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public sealed class NotFoundException : Exception
    {
        public NotFoundException(int id) : base($"The item with the id {id} was not found.")
        {
        }
    }
}
