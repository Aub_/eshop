﻿using Contracts;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Domain.Repositories
{
    public interface ICustomerRepository
    {
        public void AddCustomer(Customer customer);
        public bool EmailExists(string emailAddress);
        public Customer GetByEmail(string emailAddress);
        public Customer GetById(int id);
        public Customer Update(Customer originalCustomer, Customer updatedCustomer);
        public Customer GetByVtoken(string vtoken);
        public List<Customer> GetAll();
    }
}
