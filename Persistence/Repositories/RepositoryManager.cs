﻿using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Persistence.Context;

namespace Persistence.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<IProductRepository> _productRepository;
        private readonly Lazy<ICategoryRepository> _categoryRepository;
        private readonly Lazy<ISubCategoryRepository> _subCategoryRepository;
        private readonly Lazy<IAdminRepository> _adminRepository;
        private readonly Lazy<ICustomerRepository> _customerRepository;
        private readonly Lazy<IUnitOfWork> _unitOfWork;

        public RepositoryManager(ShopContext shopDBContext)
        {
            _productRepository = new Lazy<IProductRepository>(() => new ProductRepository(shopDBContext));
            _categoryRepository = new Lazy<ICategoryRepository>(() => new CategoryRepository(shopDBContext));
            _subCategoryRepository = new Lazy<ISubCategoryRepository>(() => new SubCategoryRepository(shopDBContext));
            _adminRepository = new Lazy<IAdminRepository>(() => new AdminRepository(shopDBContext));
            _customerRepository = new Lazy<ICustomerRepository>(() => new CustomerRepository(shopDBContext));
            _unitOfWork = new Lazy<IUnitOfWork>(() => new UnitOfWork(shopDBContext));
        }

        //not really sure what's happening here.
        public IProductRepository ProductRepository => _productRepository.Value;
        public ICategoryRepository CategoryRepository => _categoryRepository.Value;
        public ISubCategoryRepository SubCategoryRepository => _subCategoryRepository.Value;
        public IAdminRepository AdminRepository => _adminRepository.Value;
        public ICustomerRepository CustomerRepository => _customerRepository.Value;
        public IUnitOfWork UnitOfWork => _unitOfWork.Value;

    }
}
