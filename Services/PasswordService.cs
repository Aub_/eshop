﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PasswordService : IPasswordService
    {
        public bool ComparePasswords(string PasswordOne, string PasswordTwo, byte[] PasswordTwoSalt)
        {
            string HashedPasswordOne = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: PasswordOne,
                salt: PasswordTwoSalt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            if (HashedPasswordOne.Equals(PasswordTwo))
                return true;
            return false;
        }

        public string HashPassword(string Password, out byte[] PasswordSalt)
        {
            PasswordSalt = new byte[128 / 8];
            // generate a 128-bit salt using a cryptographically strong random sequence of nonzero values
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(PasswordSalt);
            }

            // derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: Password,
                salt: PasswordSalt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            return hashed;
        }
    }
}
