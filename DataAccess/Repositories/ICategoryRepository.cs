﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface ICategoryRepository
    {
        void Add(List<Category> categories);
        void Delete(List<Category> categories);
        Category GetById(int categoryId);
        List<Category> GetByIds(List<int> categoryIds);
        List<Category> GetByName(string name);
        List<Category> GetAll();
        Category Update(Category original , Category updated);
    }
}
