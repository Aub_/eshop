﻿using Contracts;
using Contracts.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface ICustomerService
    {
        public CustomerDTO GetById(int id);
        public CustomerDTO Update(CustomerForUpdateDTO updatedCustomer);
        public CustomerDTO GetByEmail(string email);
        public List<CustomerDTO> GetAll();

        // Account
        public CustomerDTO Signup(RegistrationDTO registrationDTO);
        public CustomerDTO Login(LoginDTO loginDTO);
        public string SetPasswordResetToken(CustomerDTO customer);
        public bool ResetPassword (ResetPasswordModel resetPasswordModel);
        public CustomerDTO ChangePassword(string newPassword,string oldPassword,CustomerDTO customer);
        
        
        // //Refresh Token
        public RefreshToken CreateRefreshToken(int id);
        public JWTModel RefreshToken(JWTModel oldTokens);
        public bool RevokeRefreshToken(int id);
    }
}
