﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public class ProductDTO
    {
        public int ID { get; set; }
        public int SubCatId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double PriceUSD { get; set; }
        public double PriceEU { get; set; }
        public string Color { get; set; }
        public string DeliveryEstimate { get; set; }
        public double Stars { get; set; }
        public double Size { get; set; }
        public int Quantity { get; set; }

        public int DiscountId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpadtedAt { get; set; }
    }
}
