﻿using Domain.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class SubCategoryRepository : ISubCategoryRepository
    {
        private ShopContext _shopContext;

        public SubCategoryRepository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public void Add(List<SubCategory> newSubCategories)
        {
             _shopContext.SubCategories.AddRange(newSubCategories);
        }

        public void Delete(List<SubCategory> toBeDeletedSubCategories)
        {
            _shopContext.RemoveRange(toBeDeletedSubCategories);
        }

        public List<SubCategory> GetAll()
        {
            return _shopContext.SubCategories.ToList();
        }

        public List<SubCategory> GetByCategoryId(int categoryId)
        {
            return _shopContext.SubCategories.Where(s => s.CategoryID == categoryId).ToList();
        }

        public SubCategory GetById(int id)
        {
            return _shopContext.SubCategories.Where(s => s.ID == id).FirstOrDefault();
        }

        public List<SubCategory> GetByIds(List<int> Ids)
        {
            return _shopContext.SubCategories.Where(s => Ids.Contains(s.ID)).ToList();
        }

        public List<SubCategory> GetByName(string name)
        {
            return _shopContext.SubCategories.Where(s => s.Name.Contains(name)).ToList();
        }

        public SubCategory Update(SubCategory original, SubCategory updatedSubCategory)
        {
            original.Name = updatedSubCategory.Name;
            original.CategoryID = updatedSubCategory.CategoryID;

            return original;
        }
    }
}
