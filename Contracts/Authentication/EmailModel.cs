﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contracts.Authentication
{
    public class EmailModel
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}
