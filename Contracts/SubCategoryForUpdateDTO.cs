﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contracts
{
    public class SubCategoryForUpdateDTO
    {
        [Required]
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
    }
}
