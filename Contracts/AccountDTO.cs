﻿using System;

namespace Contracts
{
    public class AccountDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }

        public Byte[] PasswordSalt { get; set; }
        public string PasswordHash { get; set; }
    }
}
