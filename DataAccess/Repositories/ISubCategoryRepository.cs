﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface ISubCategoryRepository
    {
        List<SubCategory> GetAll();
        List<SubCategory> GetByName(string name);
        SubCategory GetById(int id);
        List<SubCategory> GetByCategoryId(int categoryId);
        List<SubCategory> GetByIds(List<int> Ids);
        void Add(List<SubCategory> newSubCategories);
        SubCategory Update(SubCategory original, SubCategory updatedSubCategory);
        void Delete(List<SubCategory> toBeDeletedSubCategories);

    }
}
