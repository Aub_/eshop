﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Contracts
{
    public class SubCategoryForCreationDTO
    {
        [Required]
        public int CategoryID { get; set; }
        [Required]
        public string Name { get; set; }
        public List<ProductDTO> Products { get; set; }
    }
}
